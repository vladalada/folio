import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';


const APP_DATA = {
    profileData: {
        title: "Vlada Zhuk",
        subTitle: "Web Developer",
        profileImageUrl: "https://firebasestorage.googleapis.com/v0/b/portfolio-77428.appspot.com/o/PHOTORESUME.jpg?alt=media&token=c0a5ad4b-80d1-449a-8a86-16014fc8c5b9",
        contacts: [
            {
                label: "LinkedIn",
                iconUrl: "https://firebasestorage.googleapis.com/v0/b/portfolio-77428.appspot.com/o/linkedin_icon.png?alt=media&token=7083ee4f-944f-44ce-b48e-b54358d3c353",
                url: "https://www.linkedin.com/in/vlada-zhuk-a1711a184/",
                showLabel: false
            },
            {
                label: "vlada.obuhovich@gmail.com",
                iconUrl: "https://firebasestorage.googleapis.com/v0/b/portfolio-77428.appspot.com/o/email_icon.png?alt=media&token=4db375c1-9173-46a9-8b8e-a0dc158896fe",
                url: "mailto:vlada.obuhovich@gmail.com",
                showLabel: true
            }
        ]
    },
    projects: [
        {
            title: "Photo Blog Demo",
            description: <div>
                <p>This is a responsive photo blog web-page designed in a 3-column layout with toggling side navigation
                    bar.</p>
                <p>It is written in plain HTML5, vanilla JavaScript and CSS.</p>
                <p>Try it out on different devices!</p>
                <p>Technologies: JavaScript, HTML, CSS, Firebase (hosting), Git.</p>
            </div>
            ,
            url: "https://photo-demo.vlada.zhuk.dev/",
            imageUrl:
                "https://firebasestorage.googleapis.com/v0/b/portfolio-77428.appspot.com/o/photo_blog_screen.png?alt=media&token=589ed862-3410-4958-a5c6-78536972e344",
            sourceUrl: "https://bitbucket.org/vladalada/photoblog/src/master/"
        },
        {
            title: "Notes App Demo",
            description: <div><p>This is a Notes board application that allows you to compose, edit and delete
                notes.</p>
                <p>The app features simple and responsive design.</p>
                <p>User data is stored in local storage for simplicity.</p>
                <p>Technologies used: JavaScript, ReactJS, Firebase (hosting), CSS, Git.</p></div>,
            url: "https://notes-demo.vlada.zhuk.dev/",
            imageUrl: "https://firebasestorage.googleapis.com/v0/b/portfolio-77428.appspot.com/o/notes_screen.png?alt=media&token=1c851a23-4aca-4322-9e33-c042079cd6bd",
            sourceUrl: "https://bitbucket.org/vladalada/notes/"
        },
        {
            title: "Design Studio web page",
            description: <div><p>This is a demo web page for interior design studio.</p>
                <p>This is a responsive web page written as a CSS Flex-box Layout.</p>
                <p>Technologies: JavaScript, React JS, HTML, CSS, Firebase (hosting), Git.</p></div>,
            url: "https://interior-demo.vlada.zhuk.dev",
            imageUrl: "https://firebasestorage.googleapis.com/v0/b/portfolio-77428.appspot.com/o/interiorScreen.png?alt=media&token=45199cb1-1a8d-4dc7-9f59-2cfc162f1c32",
            sourceUrl: ""
        },
        // {
        //     title: "Snaps&Gobble Books email",
        //     description: <div><p>Simple email template for an imaginary book store.</p>
        //         <p>Features simple table layout.</p></div>,
        //     url: "https://email-demo.vlada.zhuk.dev/",
        //     imageUrl: "https://firebasestorage.googleapis.com/v0/b/portfolio-77428.appspot.com/o/email_template.jpg?alt=media&token=6db434bd-71a3-460e-ba43-8f23389a5677",
        //     sourceUrl: ""
        // },
        {
            title: "California Travel Guide",
            description: <div><p>This is a responsive web-page for traveling magazine
                designed in two-column layout with top navigation bar.</p>
                <p>Technologies: HTML5, CSS3, Firebase (hosting)</p>
            </div>,
            url: "https://travel-demo.vlada.zhuk.dev/",
            imageUrl: "https://firebasestorage.googleapis.com/v0/b/portfolio-77428.appspot.com/o/travel_guide.jpg?alt=media&token=53187e81-a60c-4489-9344-596d59036d38",
            sourceUrl: "https://bitbucket.org/vladalada/travelguide"
        }
    ]
};

class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const contacts = [];
        for (let i = 0; i < this.props.appData.profileData.contacts.length; i++) {
            const contact = this.props.appData.profileData.contacts[i];
            let label = undefined;
            if (contact.showLabel) {
                label = <p>{contact.label}</p>;
            }
            contacts.push(<div className={"contact"}>
                <a className={"contactUrl"} href={contact.url} target="_blank">
                    <img className={"contactIcon"} src={contact.iconUrl} alt={contact.label}/>
                </a>
                {label}
            </div>);
        }
        const projects = [];
        for (let i = 0; i < this.props.appData.projects.length; i++) {
            const project = this.props.appData.projects[i];
            projects.push(<div className={"project"}>
                <div className={"projectInfo"}>
                    <a href={project.url} target="_blank">
    <span className={"projectText"}>
    <h1 className={"projectTitle"}>{project.title}</h1>
    <p className={"projectDescription"}>{project.description}</p>
    </span>
                        <img className={"projectImage"} src={project.imageUrl} alt={project.title}/>
                    </a>
                    <a className={"projectSource"} href={project.sourceUrl} target="_blank"><i>Source code</i></a>
                </div>
            </div>);
        }
        return <div className={"App"}>
            <div className={"profileColumn"}>
                <div className={"profile"}>
                    <img className={"profileImage"} src={this.props.appData.profileData.profileImageUrl}
                         alt={"Profile Image"}/>
                    <h1 className={"profileTitle"}>{this.props.appData.profileData.title}</h1>
                    <h2 className={"profileSubTitle"}>{this.props.appData.profileData.subTitle}</h2>
                </div>
                <div className={"contacts"}>
                    {contacts}
                </div>
            </div>
            <div className={"projectsColumn"}>
                <h1 className={"projectsHeader"}>Projects</h1>
                {projects}
            </div>
        </div>
    }
}

ReactDOM.render(
    <App appData={APP_DATA}/>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
